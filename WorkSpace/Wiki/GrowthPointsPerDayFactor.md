# ThingDefExtension_GrowthPointsPerDayFactor

```xml
<Defs>
	<ThingDef>
		<modExtensions>
			<li Class="Ab_Utility.ThingDefExtension_GrowthPointsPerDayFactor" MayRequire="Ludeon.RimWorld.Biotech">
				<growthPointsPerDayFactor>1</growthPointsPerDayFactor>
				<growthPointsPerDayVatFactor>1</growthPointsPerDayVatFactor>
			</li>
		</modExtensions>
	</ThingDef>
</Defs>
```
Above shows the default values of the mod extension.


Both `<growthPointsPerDayFactor />` and `<growthPointsPerDayVatFactor />` multply the point attainment value with the only exception being that they are mutually exclusive on when they operate where `<growthPointsPerDayVatFactor />` only takes effect for pawns inside growthVat buildings as defined by the class. 

The patch logic does not run either if BioTech is not loaded or if the `<ThingDef />` does not have the mod extension.