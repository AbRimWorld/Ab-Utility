# [Features](Features)
- A listing of features without tutorials or guidance.
## [Hediff stage]

## [Quests]
### [QuestNode_GenerateRandomPawn](https://gitlab.com/AbRimWorld/Ab-Utility/-/wikis/Quests#questnode_generaterandompawn)
- Choose from a List of pawns to generate for a quest Joiner pawn.

## [Hediffs]
### [HediffCompProperties_GiveAbilities]()
- Choose a list of AbilityDefs to give to a pawn when a hediff is at a certain stage
 - As of 1.4 Vanilla game also has a hediff function like this but it does not rely on stages. Search for the `<comp>` named `HediffCompProperties_GiveAbility`.