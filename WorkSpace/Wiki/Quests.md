# QuestNode_GenerateRandomPawn
Choose a pawn from a whiteList to spawn. Replaces [QuestNode_GeneratePawn]() from Core.\
Our example will cover [Util_JoinerWalkIn]() but it works in more cases.\
Original Core code:
```xml
	<QuestScriptDef>
		<defName>Util_JoinerWalkIn</defName>
		<questDescriptionRules>
			<rulesStrings>
				<li>rewardDescription->a [joiner_age]-year-old [joiner_title] named [joiner_nameDef] will arrive and join you. [joiner_relationInfo]</li>
			</rulesStrings>
		</questDescriptionRules>
		<root Class="QuestNode_Sequence">
			<nodes>
				<li Class="QuestNode_GeneratePawn">
					<storeAs>joiner</storeAs>
					<kindDef>SpaceRefugee</kindDef>
				</li>

				<li Class="QuestNode_PawnsArrive">
					<pawns>$joiner</pawns>
					<joinPlayer>true</joinPlayer>
					<customLetterLabel>$customLetterLabel</customLetterLabel>
					<customLetterText>$customLetterText</customLetterText>
					<customLetterLabelRules>$customLetterLabelRules</customLetterLabelRules>
					<customLetterTextRules>$customLetterTextRules</customLetterTextRules>
					<isSingleReward>true</isSingleReward>
					<rewardDetailsHidden>true</rewardDetailsHidden>
				</li>
			</nodes>
		</root>
	</QuestScriptDef>
```

Patch within Ab Utility changes the whole node of `<li Class="QuestNode_GeneratePawn">` to:
```xml
			<li Class="Ab_Utility.QuestNode_GenerateRandomPawn">
				<storeAs>joiner</storeAs>
				<weightedPawnKinds>
					<li>
						<key>SpaceRefugee</key>
						<value>10</value>
					</li>
				</weightedPawnKinds>
			</li>
```
`<key>`(pawnKindDef) takes a [pawnKindDef](Glossary) defName, and is the choice to spawn.\
`<value>`(float) is a weight, the higher it is the more likely the game will chose it.

To add more items to this list or similar above use a patch as below. Where you would change the Xpath and the `<li>` entries.
```xml
	<Operation Class="PatchOperationAdd">
		<xpath>Defs/QuestScriptDef[defName="Util_JoinerWalkIn"]/root[@Class="QuestNode_Sequence"]/nodes/li[@Class="Ab_Utility.QuestNode_GenerateRandomPawn"]/weightedPawnKinds</xpath>
		<value>
			<li>
				<key>MyPawnKind</key>
				<value>2</value>
			</li>
		</value>
	</Operation>
```
Patched Defs with `QuestNode_GenerateRandomPawn`:
- Util_JoinerWalkIn (Core)
- Util_JoinerDropIn (Core)

\
If you are interested in the C# then look [HERE]()