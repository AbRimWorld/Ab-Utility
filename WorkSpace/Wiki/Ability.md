# Ability

## ~~Extension_Projectile~~ {Deprecated}
See `Defs/AbilityDef/comps` and class name `CompProperties_AbilityLaunchProjectile`.

Spawns a projectile from Caster pawns to Target pawn

A def `modExtension` for `<AbilityDef>` which takes any projetctile Def and casts it (Example uses the gernade from Core).

Requires setting the `<abilityClass>` to `Ab_Utility.Ability_Projectile` .

```xml
	<AbilityDef>
		.............
		<abilityClass>Ab_Utility.Ability_Projectile</abilityClass>
		.............
		<modExtensions>
			<li Class="Ab_Utility.Extension_Projectile">
				<projectile>Proj_GrenadeFrag</projectile>
			</li>
		</modExtensions>
	</AbilityDef>
```
- `<projectile>`(ThingDef) Takes any projectile Def.


## ~~Extension_GiveRacialAbilities~~ {Disabled, Deprecated}
See `Defs/PawnKindDef/abilities`.

Gives members of a Race a list of abilities

works for `<ThingDef>` and any **Type** that inherits from it, this includes `<AlienRace.ThingDef_AlienRace>` .

```xml
	<ThingDef>
		.............
		<modExtensions>
			<li Class="Ab_Utility.Extension_GiveRacialAbilities">
				<playerOnly>False</playerOnly>
				<genderFilter />
				<abilityDefs>
					<li>YourAbilityDef</li>
				</abilityDefs>
			</li>
		</modExtensions>
	</ThingDef>
```
- `<playerOnly>`(bool) determins if only Pawns spawned as members of the player faction should get the ability list.

Omiting this results in `<playerOnly>False</playerOnly>` as well.

- `<genderFilter />`(Enum) Takes the values of None, Male, Female. 

If None is selected or if the value is left empty as show in the example above, or if the node is not even included all Genders of said Race will be able to get the ability. 

If Male is chosen only Males of said Race will get the ability list, The same applied for selecting Female.

- `<abilityDefs>`(List of AbilityDef) Takes a list of Ability Defs, these Defs will be assigned to the Pawn.

## ~~Extension_GivePawnkindAbilities~~ {Disabled, Deprecated}
See `Defs/PawnKindDef/abilities`.

Gives members of pawnKind a list of abilities

works for any `<PawnKindDef>` .

```xml
	<PawnKindDef>
		.............
		<modExtensions>
			<li Class="Ab_Utility.Extension_GivePawnkindAbilities">
				<playerOnly>False</playerOnly>
				<genderFilter />
				<abilityDefs>
					<li>YourAbilityDef</li>
				</abilityDefs>
			</li>
		</modExtensions>
	</PawnKindDef>
```

- Same as `Extension_GiveRacialAbilities`

## HediffComp_GiveAbilities
- Hediff stages give a list of abilities.

```xml
	<HediffDef>
		.............
		<stages>
			<li>
				<label>0</label>
				<becomeVisible>false</becomeVisible>
			</li>
			<li>
				<minSeverity>0.50</minSeverity>
				<label>1</label>
			</li>
			<li>
				<minSeverity>0.60</minSeverity>
				<label>2</label>
			</li>
		</stages>
		<comps>
			<li Class="Ab_Utility.HediffCompProperties_GiveAbilities">
				<stageAbilityDefs>
					<li>
						<key>0</key>
						<value>
							<li>YourAbility1</li>
						</value>
					</li>
					<li>
						<key>1</key>
						<value>
							<li>YourAbility1</li>
							<li>YourAbility2</li>
						</value>
					</li>
				</stageAbilityDefs>
			</li>
		</comps>
	</HediffDef>
```

- `<stageAbilityDefs>`(List) Is a container which holds our list elements.

- `<key>`(Int) Refer to the stage of the Hediff where `0` is the first stage and every following stage incriments by `1` .

- `<value>`(List of Ability Defs) Takes a list of Ability Defs, these Defs will be assigned to the Pawn with the hediff of matching stage.

Note that previous stage Abilities are not added onto but rather the list is replaced, as such in the given example above on Stage `3` of the hediff there are no abilites.