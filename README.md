# About the Mod
This is a Library and Asset mod utilized by my other mods.
It adds some new features which I only make use of in other mods, and it stores Assets which are shared across more than one mod.

Like my work ?\
If you want to support me I have a [Ko-Fi](https://ko-fi.com/abraxas_).