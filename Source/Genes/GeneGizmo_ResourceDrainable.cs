﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.Sound;

namespace Ab_Utility
{
	public class GeneGizmo_ResourceDrainable : GeneGizmo_Resource
	{

		private List<Pair<IGeneResourceDrain, float>> tmpDrainGenes = new List<Pair<IGeneResourceDrain, float>>();

		public GeneGizmo_ResourceDrainable(Gene_Resource gene, List<IGeneResourceDrain> drainGenes, Color barColor, Color barhighlightColor)
			: base(gene, drainGenes, barColor, barhighlightColor)
		{
#if v1_4
			draggableBar = false;
#endif
		}
#if !v1_4
		protected override bool IsDraggable => false;
		protected override IEnumerable<float> GetBarThresholds()
		{
			yield break;
		}
#endif
		protected override string GetTooltip()
		{
			tmpDrainGenes.Clear();
			string currentValueLabel = $"{gene.ResourceLabel.CapitalizeFirst().Colorize(ColoredText.TipSectionTitleColor)}: {gene.ValueForDisplay} / {gene.MaxForDisplay}\n";
			if (!drainGenes.NullOrEmpty())
			{
				float valueOffset = 0f;
				foreach (IGeneResourceDrain drainGene in drainGenes)
				{
					if (drainGene.CanOffset)
					{
						tmpDrainGenes.Add(new Pair<IGeneResourceDrain, float>(drainGene, drainGene.ResourceLossPerDay));
						valueOffset += drainGene.ResourceLossPerDay;
					}
				}
				if (valueOffset != 0f)
				{
					string offsetSuffixLabel = (valueOffset < 0f) ? "RegenerationRate".Translate() : "DrainRate".Translate();
					currentValueLabel += $"\n\n {offsetSuffixLabel}: {"PerDay".Translate(Mathf.Abs(gene.PostProcessValue(valueOffset))).Resolve()}";
					foreach (Pair<IGeneResourceDrain, float> tmpDrainGene in tmpDrainGenes)
					{
						currentValueLabel += $"\n  - {tmpDrainGene.First.DisplayLabel.CapitalizeFirst()} {"PerDay".Translate(gene.PostProcessValue(0f - tmpDrainGene.Second).ToStringWithSign()).Resolve()}";
					}
				}
			}
			if (!gene.def.resourceDescription.NullOrEmpty())
			{
				currentValueLabel += $"\n\n {gene.def.resourceDescription.Formatted(gene.pawn.Named("PAWN")).Resolve()}";
			}
			return currentValueLabel;
		}
	}
}