﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Verse;

namespace Ab_Utility
{
	public class PatchOperationConfigMatch : PatchOperation
	{
		private string settingsKey;
		private PatchOperation match;
		private PatchOperation nomatch;
		protected override bool ApplyWorker(XmlDocument xml)
		{
			bool value = Ab_Mod.settings.IsKeyedSettingEnabled(settingsKey);
			if (value)
			{
				if (match != null)
				{
					return match.Apply(xml);
				}
			}
			else if (nomatch != null)
			{
				return nomatch.Apply(xml);
			}
			if (match == null)
			{
				return nomatch != null;
			}
			return true;
		}
	}
}