﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class StatPart_HediffFavorValue : StatPart
	{
		public override string ExplanationPart(StatRequest req)
		{
			if (!Ab_Mod.settings.RoyalFavorImpact)
			{
				return null;
			}
			Pawn pawn = req.Thing as Pawn;
			if (pawn == null)
			{
				return null;
			}
			List<Hediff> hediffs = pawn.health?.hediffSet?.hediffs;
			if (hediffs == null)
			{
				return null;
			}
			List<string> subExplanation = new List<string>();
			float totalOffset = 0;
			foreach (Hediff hediff in hediffs)
			{
				ThingDef thingDef = hediff.def.spawnThingOnRemoved;
				if (thingDef == null)
				{
					continue;
				}
				float favorOffset = thingDef.GetStatValueAbstract(StatDefOf.RoyalFavorValue);
				if (favorOffset <= 0)
				{
					continue;
				}
				totalOffset += favorOffset;
				string label = hediff.Label;
				subExplanation.Add($"    {label}: +{favorOffset}");
			}
			if (subExplanation.NullOrEmpty())
			{
				return null;
			}
			string explanation = $"{"StatsReport_Health".Translate()}: +{totalOffset}\n{string.Join("\n", subExplanation)}\n";
			return explanation;
		}
		public override void TransformValue(StatRequest req, ref float val)
		{
			if (!Ab_Mod.settings.RoyalFavorImpact)
			{
				return;
			}
			Pawn pawn = req.Thing as Pawn;
			if (pawn == null)
			{
				return;
			}
			List<Hediff> hediffs = pawn.health?.hediffSet?.hediffs;
			if (hediffs == null)
			{
				return;
			}
			foreach (Hediff hediff in hediffs)
			{
				ThingDef thingDef = hediff.def.spawnThingOnRemoved;
				if (thingDef == null)
				{
					continue;
				}
				float favorOffset = thingDef.GetStatValueAbstract(StatDefOf.RoyalFavorValue);
				if (favorOffset <= 0)
				{
					continue;
				}
				val += favorOffset;
			}
		}
	}
}