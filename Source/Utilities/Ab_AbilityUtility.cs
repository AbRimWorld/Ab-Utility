﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public static class Ab_AbilityUtility
	{
		public static AbilityDef PsylinkAbilityFor(Pawn pawn, int level)
		{
			List<AbilityDef> currentAbilities = pawn.abilities.AllAbilitiesForReading.Select(ab => ab.def).ToList();
			List<AbilityDef> allAvailableAbilities = DefDatabase<AbilityDef>.AllDefsListForReading
				.FindAll(def =>
					def.level != 0 
					&& def.level <= level
					&& !currentAbilities.Contains(def)
				);
			List<AbilityDef> currentLevelAbilities = allAvailableAbilities.FindAll(def => def.level == level);
			if (currentLevelAbilities.Any())
			{
				return currentLevelAbilities.RandomElementWithFallback();
			}

			return allAvailableAbilities.RandomElementWithFallback();
		}
		public static void GainRandomPsylinkAbility(Pawn pawn, int level)
		{
			AbilityDef ability = PsylinkAbilityFor(pawn, level);
			if (ability != null)
			{	 
				pawn.abilities.GainAbility(ability);

				string text = "Ab_PawnGainedPsyAbility".Translate(pawn.Named("PAWN"), ability.Named("ABILITY"));
				Messages.Message(text, MessageTypeDefOf.PositiveEvent);
			}
			else
			{
				Log.Warning("no abilities to give");
			}
			
		}
		public static void GainPsylink(Pawn pawn)
		{
			Hediff_Level hediff_Level = pawn.GetMainPsylinkSource();
			if (hediff_Level == null)
			{
				hediff_Level = HediffMaker.MakeHediff(HediffDefOf.PsychicAmplifier, pawn, pawn.health.hediffSet.GetBrain()) as Hediff_Level;
				pawn.health.AddHediff(hediff_Level);
				return;
			}
			hediff_Level.ChangeLevel(1);
		}
	}
}