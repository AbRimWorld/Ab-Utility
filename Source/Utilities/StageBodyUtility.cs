﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public static class StageBodyUtility
	{
		public static bool HasHediffStageBodies(Pawn pawn)
		{
			if (pawn.health == null)
			{
				return false;
			}
			return pawn.health.hediffSet.hediffs.Any(hed => hed.def.HasModExtension<HediffDefExtension_SeverityBody>());
		}
		public static StageBody GetActiveStageBody(Pawn pawn)
		{
			if (pawn.health == null)
			{
				return null;
			}
			IEnumerable<Hediff> validHediffs = pawn.health.hediffSet.hediffs
				.Where(hed => hed.def.HasModExtension<HediffDefExtension_SeverityBody>())
				.OrderByDescending(hed => hed.def.GetModExtension<HediffDefExtension_SeverityBody>().Priority);
			if (validHediffs.EnumerableNullOrEmpty())
			{
				return null;
			}
			foreach (Hediff hediff in validHediffs)
			{
				StageBody body = hediff.def.GetModExtension<HediffDefExtension_SeverityBody>().CurrentStageBody(hediff);
				if (body != null)
				{
					return body;
				}
			}
			return null;
		}
	}
}