﻿using RimWorld.QuestGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	class QuestNode_GenerateRandomPawn : QuestNode_GeneratePawnRandDevelopmentStage
	{
		//List<PawnKindDef> kindDefList;
		Dictionary<PawnKindDef, float> weightedPawnKinds;

		private PawnKindDef ChoosePawnKind()
        {
			return weightedPawnKinds.RandomElementByWeight(kvp => kvp.Value).Key;
		}

		protected override void RunInt()
		{
			//kindDef = kindDefList.RandomElementWithFallback();// was true random.
			kindDef = ChoosePawnKind();
			if (kindDef == null)
			{
				Log.Message("QuestNode_GenerateRandomPawn kindDef is null");
				return;  // error here would be appropriatea
			}
			base.RunInt();
		}
	}
}