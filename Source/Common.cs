﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Ab_Utility
{
	public static class Common
	{
		[MayRequireRoyalty]
		public static ThoughtDef AnimaScream = DefDatabase<ThoughtDef>.GetNamed("AnimaScream");

		//ToDo : mod and DLC detection
		public static HediffDef Ab_DrainBuff = DefDatabase<HediffDef>.GetNamed("Ab_DrainAnimaBuff");
		public static HediffDef Ab_DrainDebuff = DefDatabase<HediffDef>.GetNamed("Ab_DrainAnimaDebuff");

		[MayRequireBiotech]
		public static GeneDef Ab_ToxGene = DefDatabase<GeneDef>.GetNamed("Ab_ToxGene");//To DefOf
		[MayRequireBiotech]
		public static GeneDef Ab_ToxNails = DefDatabase<GeneDef>.GetNamed("Ab_UngualVenomGlands");//To DefOf // ToDo: Check if this is even used.



		//public static bool isDemiFoxLoaded = ModLister.AnyFromListActive(new List<string>() { "Abraxas.DemiFox.Race" }); //Example of mod detection
	}

	[StaticConstructorOnStartup]
	public static class Textures
	{
		//[MayRequireRoyalty]
		//public static Texture2D Psylink = ContentFinder<Texture2D>.Get("UI/Icons/Rituals/AnimaTreeLinking");
		public static Texture2D TreeToPsylink = ContentFinder<Texture2D>.Get("Ab/UI/Icons/TreeToPsylink");
		public static Texture2D TreeToAbility = ContentFinder<Texture2D>.Get("Ab/UI/Icons/TreeToAbility");
		public static Texture2D AnimaBuff = ContentFinder<Texture2D>.Get("Ab/UI/Icons/AnimaBuff");
		public static Texture2D AnimaDrain = ContentFinder<Texture2D>.Get("Ab/UI/Icons/AnimaDrain");
		public static Texture2D AnimaDeposite = ContentFinder<Texture2D>.Get("Ab/UI/Icons/AnimaDeposite");
	}
}
