﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	//ToDo: Revist this or remove it
	public class Extension_GiveInnateAbilities : DefModExtension
	{
		public bool playerOnly = false;
		public Gender? genderFilter = null;
		public List<AbilityDef> abilityDefs = new List<AbilityDef>();

		public bool CanGiveAbilityToPawn(Pawn pawn)
		{
			//bool controlCheck = pawn.IsColonistPlayerControlled;
			//bool secondCheck = pawn.IsColonist;
			//bool thirdCheck = pawn.Faction.IsPlayer;
			//Log.Message("is player controlled = " + controlCheck.ToString());
			if (playerOnly && !pawn.Faction.IsPlayer)
			{
				//Log.Message("==== Player Only set to true, and pawn is not player controlled");
				//Log.Message("player only " + playerOnly.ToString());
				return false;
			}
			if (!IsGenderAccepted(pawn.gender))
			{
				//Log.Message("Wrong Pawn gender");
				return false;
			}
			return true;
		}
		private bool IsGenderAccepted(Gender gender)
		{
			//Log.Message($"Gender filter: {genderFilter} is null ? {genderFilter == null}");
			if (genderFilter == null)// treat null as GenderLess and Any
			{
				//Log.Message("null to true");
				return true;
			}
			return gender == genderFilter.Value;
		}

		public override IEnumerable<string> ConfigErrors()
		{
			foreach (string error in base.ConfigErrors())
			{
				yield return error;
			}
			if (abilityDefs.NullOrEmpty())
			{
				yield return ("required list \"abilityDefs\" with at least one value");
			}
		}
	}

	public class ThingDefExtension_GiveRacialAbilities : Extension_GiveInnateAbilities {}
	public class PawnKindDefExtension_GivePawnkindAbilities : Extension_GiveInnateAbilities {}
}
