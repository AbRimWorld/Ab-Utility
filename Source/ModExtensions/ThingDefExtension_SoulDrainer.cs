﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class ThingDefExtension_SoulDrainer : DefModExtension
	{
		List<SoulDrainer> soulDrainer = new List<SoulDrainer>();
		public SoulDrainer SoulDrainerFor(Pawn pawn)
		{
			LifeStageDef lifeStage = pawn.ageTracker?.CurLifeStage;
			if (lifeStage == null)
			{
				return null;
			}
			SoulDrainer activeSoulDrainer = soulDrainer.Find(sd => sd.lifeStage == lifeStage);
			return activeSoulDrainer;
		}
	}
	public class SoulDrainer
	{
		public LifeStageDef lifeStage = null;
		Gender initiatorGender = Gender.None;
		Gender targetGender = Gender.None;
		bool canDrainInsect = false;
		bool canDrainAnimal = false;
		bool canDrainHumanLike = false;
		bool isSelfImmuneToSoulDrain = false;
		List<ThingDef> drainableThings = new List<ThingDef>();

#region Bool Checks
		public bool CanDrain(Pawn pawn, Thing target)
		{
			if (!InitiatorCanDrain(pawn))
			{
				return false;
			}
			if (target is Pawn targetPawn)
			{
				if (!CanDrainTargetPawn(targetPawn))
				{
					return false;
				}
			}
			else
			{
				if (!CanDrainTarget(target))
				{
					return false;
				}
			}
			return true;
		}
		private bool InitiatorCanDrain(Pawn pawn)
		{
			return pawn.gender.Matches(initiatorGender);
		}
		private bool CanDrainTargetPawn(Pawn targetPawn)
		{
			if (!targetGender.Matches(targetPawn.gender))
			{
				return false;
			}
			if (targetPawn.RaceProps.FleshType == FleshTypeDefOf.Mechanoid)
			{
				return false;
			}
			if (targetPawn.RaceProps.FleshType == FleshTypeDefOf.Insectoid && !canDrainInsect)
			{
				return false;
			}
			if (targetPawn.RaceProps.Animal && !canDrainAnimal)
			{
				return false;
			}
			if (targetPawn.RaceProps.Humanlike && !canDrainHumanLike)
			{
				return false;
			}
			SoulDrainer targetDrainer = targetPawn.def.GetModExtension<ThingDefExtension_SoulDrainer>()?.SoulDrainerFor(targetPawn);
			if (targetDrainer != null && targetDrainer.isSelfImmuneToSoulDrain)
			{
				return false;
			}
			return true;
		}
		private bool CanDrainTarget(Thing thing)
		{
			return drainableThings.Contains(thing.def);
		}
#endregion
	}
}