﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
    public class HediffDefExtension_SeverityBody : DefModExtension
    {
        public int Priority = 1;
        Dictionary<int, StageBody> stageBodies = new Dictionary<int, StageBody>();

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (stageBodies.NullOrEmpty())
            {
                yield return "stageBodies is empty";
            }
            else
            {
                foreach (StageBody body in stageBodies.Values)
                {
                    foreach (string error in body.ConfigErrors())
                    {
                        yield return error;
                    }
                }
            }
        }
        public StageBody CurrentStageBody(Hediff hediff)
        {
            return stageBodies.TryGetValue(hediff.CurStageIndex);
        }
    }
    public class StageBody
    {
        public BodyDef bodyDef;
        GraphicData graphicData;// Should this be removed since Genes exist ? perhapse it can be salvaged as a bodyType forcer instead.
        public Graphic Graphic => graphicData?.Graphic;
        public IEnumerable<string> ConfigErrors()
        {
            if(bodyDef == null)
            {
                yield return "no bodyDef defined";
            }
            if (graphicData != null)
            {
                if (graphicData.texPath == null)
                yield return "no graphics defined";
            }
        }
    }
}
