﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	[HarmonyPatch(typeof(PawnGenerator), "GeneratePawn", new Type[] {typeof(PawnGenerationRequest) })]
	public static class RaceForcedGenes_Patch
	{
		[HarmonyPostfix]
		public static void AddForcedRaceEndoGenes(Pawn __result)
		{
			if (__result == null)
			{
				return;
			}
			if (__result.DevelopmentalStage.Newborn() || __result.DevelopmentalStage.Baby())
			{
				return;
			}
			ThingDefExtension_ForcedGenes modExtension = __result.def.GetModExtension<ThingDefExtension_ForcedGenes>();
			if (modExtension == null)
			{
				return;
			}
			List<GeneDef> genes = modExtension.GetRandomGenes();
			foreach(GeneDef gene in genes)
			{
				__result.genes.AddGene(gene, false);
			}
		}
	}
}