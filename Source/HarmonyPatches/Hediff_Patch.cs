﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
    [HarmonyPatch(typeof(Hediff), "get_CurStageIndex")]
    public static class Hediff_Patch
    {
        private static Dictionary<Hediff, int> cachedHediffStages = new Dictionary<Hediff, int>();
        [HarmonyPostfix]
        static void CompareToCachedHediffStage(int __result, Hediff __instance)
        {
            HediffComp_GiveAbilities comp = __instance.TryGetComp<HediffComp_GiveAbilities>();
            if (comp == null)
            {
                return;
            }
            if (!cachedHediffStages.ContainsKey(__instance))
            {
                cachedHediffStages.Add(__instance, __result);
            }
            if (cachedHediffStages.TryGetValue(__instance) == __result)
            {
                return;
            }
            comp.CompPostStageChange();
            cachedHediffStages.SetOrAdd(__instance, __result);
        }
    }
}