﻿using HarmonyLib;
using System.Xml;
using Verse;

namespace Ab_Utility
{
	/// <summary>
	///	This patch is called manually in Settings
	/// </summary>
	public static class UnloadDefs_Patch
	{
		public static int nodecount;

		[HarmonyPrefix]
		public static bool ShouldUnloadDef(XmlNode node, LoadableXmlAsset loadingAsset, ref Def __result)
		{
			nodecount++;
			if (node.NodeType != XmlNodeType.Element)
			{
				return true;
			}
			if (!Ab_Mod.settings.DevAssets && (node.Attributes?["Ab_isDevAsset"] != null))
			{
				__result = null;
				return false;
			}
			if (!Ab_Mod.settings.CustomStartScenarios && (node.Attributes?["Ab_isPlayerStart"] != null))
			{
				__result = null;
				return false;
			}
			if (!Ab_Mod.settings.IsKeyedSettingEnabled(Settings.EnableProstheticsKey) && (node.Attributes?["Ab_isProstheticContent"] != null))
			{
				__result = null;
				return false;
			}
			return true;
		}
	}
}