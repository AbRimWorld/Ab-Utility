﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	[HarmonyPatch(typeof(Pawn_AgeTracker), "GrowthPointsPerDay", MethodType.Getter)] // get_PropertyName is an atrifact. In theory [HarmonyPatch("PropertyName", MethodType.Getter)] would also have worked.
	public static class GrowthPointsPerDayFactor_Patch
	{
		[HarmonyPostfix]
		static void ApplyRaceGrowthPointsPerDayFactor(Pawn_AgeTracker __instance, ref float __result, ref Pawn ___pawn)
		{
			if (ModsConfig.BiotechActive) // Due to MayRequire on XML this is not needed, however should skip entering the following Logic with less cost than GetModExtension
			{
				ThingDefExtension_GrowthPointsPerDayFactor extension = ___pawn.kindDef.race.GetModExtension<ThingDefExtension_GrowthPointsPerDayFactor>();
				if (extension != null)
				{
					if (___pawn.ParentHolder is RimWorld.Building_GrowthVat)
					{
						__result *= extension.growthPointsPerDayVatFactor;
					}
					else
					{
						__result *= extension.growthPointsPerDayFactor;
					}
				}
			}
		}
	}
}