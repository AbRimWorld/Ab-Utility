﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	[DefOf]
	public static class Ab_GeneDefOf
	{
		[MayRequireBiotech]
		public static GeneDef Ab_ToxGene;

		[MayRequireBiotech]
		public static GeneDef Ab_UngualVenomGlands;

		static Ab_GeneDefOf()
		{
			DefOfHelper.EnsureInitializedInCtor(typeof(Ab_GeneDefOf));
		}
	}
}