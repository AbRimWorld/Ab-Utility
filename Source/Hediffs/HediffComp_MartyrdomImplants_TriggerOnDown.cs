﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ab_Utility
{
	public class HediffComp_MartyrdomImplants_TriggerOnDown : HediffComp_MartyrdomImplants
	{
		private HediffCompProperties_MartyrdomImplants_TriggerOnDown Props => (HediffCompProperties_MartyrdomImplants_TriggerOnDown)props;

		public override void CompPostTick(ref float severityAdjustment)
		{
			base.CompPostTick(ref severityAdjustment);
			if (Pawn.Downed)
			{
				SetDelayedEffectTick();
				TriggerOutcome();
			}
		}
		public override void DoOutcome()
		{
			PlayVisuals();
			SpawnFilth();
			PlaySound();
			DestroyHediffOnTrigger();
			ApplyHediffGiver();
			SpawnExplosionOnSelf();
		}
	}

	public class HediffCompProperties_MartyrdomImplants_TriggerOnDown : HediffCompProperties_MartyrdomImplants
	{
		public HediffCompProperties_MartyrdomImplants_TriggerOnDown()
		{
			compClass = typeof(HediffComp_MartyrdomImplants_TriggerOnDown);
		}
	}
}