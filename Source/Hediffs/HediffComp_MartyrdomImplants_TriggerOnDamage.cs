﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class HediffComp_MartyrdomImplants_TriggerOnDamage : HediffComp_MartyrdomImplants
	{
		private HediffCompProperties_MartyrdomImplants_TriggerOnDamage Props => (HediffCompProperties_MartyrdomImplants_TriggerOnDamage)props;

		public override void Notify_PawnPostApplyDamage(DamageInfo dinfo, float totalDamageDealt)
		{
			base.Notify_PawnPostApplyDamage(dinfo, totalDamageDealt);
			if (Props.triggerOnDamageChance <= 0f)
			{
				return;
			}
			if (!ShouldTrigger())
			{
				return;
			}
			SetDelayedEffectTick();
		}

		private bool ShouldTrigger()
		{
			return Rand.Chance(Props.triggerOnDamageChance);
		}

		public override void DoOutcome()
		{
			PlayVisuals();
			SpawnFilth();
			PlaySound();
			DestroyHediffOnTrigger();
			ApplyHediffGiver();
			SpawnExplosionOnSelf();
		}
	}

	public class HediffCompProperties_MartyrdomImplants_TriggerOnDamage : HediffCompProperties_MartyrdomImplants
	{
		public float triggerOnDamageChance = 1f;
		public HediffCompProperties_MartyrdomImplants_TriggerOnDamage()
		{
			compClass = typeof(HediffComp_MartyrdomImplants_TriggerOnDamage);
		}
	}
}