﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Ab_Utility
{
	public class HediffComp_GiveAbilities : HediffComp
	{
		private HediffCompProperties_GiveAbilities Props => (HediffCompProperties_GiveAbilities)props;
		List<AbilityDef> addedAbilities = new List<AbilityDef>();
		bool stageChangeAnnounced = false;
		public override void CompPostPostAdd(DamageInfo? dinfo)
		{
			AddAbilities();
		}
		public override void CompPostPostRemoved()
		{
			RemoveAbilities();
		}
		public override void CompPostTick(ref float severityAdjustment)
		{
			base.CompPostTick(ref severityAdjustment);
			if (stageChangeAnnounced)
			{
				RemoveAbilities();
				AddAbilities();
				stageChangeAnnounced = false;
			}
		}
		public void CompPostStageChange()
		{
			stageChangeAnnounced = true;
		}
		private void AddAbilities()
		{
			foreach (AbilityDef ability in Props.CurrentAbilityDefs(base.parent))
			{
				bool hasAbility = parent.pawn.abilities.GetAbility(ability) != null;
				if (!hasAbility)
				{
					parent.pawn.abilities.GainAbility(ability);
					addedAbilities.Add(ability);
				}
			}
		}
		private void RemoveAbilities()
		{
			foreach (AbilityDef ability in addedAbilities)
			{
				parent.pawn.abilities.RemoveAbility(ability);
			}
			addedAbilities.Clear();
		}
	}
	public class HediffCompProperties_GiveAbilities : HediffCompProperties
	{
		//public List<AbilityDef> abilityDefs = new List<AbilityDef>();
		Dictionary<int, List<AbilityDef>> stageAbilityDefs = new Dictionary<int, List<AbilityDef>>();
		public HediffCompProperties_GiveAbilities()
		{
			compClass = typeof(HediffComp_GiveAbilities);
		}

		public List<AbilityDef> CurrentAbilityDefs(Hediff hediff)
		{
			List<AbilityDef> stageValidation = stageAbilityDefs.TryGetValue(hediff.CurStageIndex);
			if (stageValidation == null)
			{
				return new List<AbilityDef>();
			}
			return stageValidation;
		}

		public override IEnumerable<string> ConfigErrors(HediffDef parentDef)
		{
			foreach(string error in base.ConfigErrors(parentDef))
			{
				yield return error;
			}
			if(stageAbilityDefs.Count == 0)
			{
				yield return "required list \"abilityDefs\" is empty";
			}
		}
	}
}
