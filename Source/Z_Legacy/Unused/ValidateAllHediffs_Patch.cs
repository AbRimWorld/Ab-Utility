﻿//using AlienRace;
//using HarmonyLib;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Verse;

//namespace Ab_Utility
//{
//	[HarmonyPatch(typeof(TickManager), "DoSingleTick")]
//	class Patch_TickManager
//	{
//		[HarmonyPostfix]
//		static void ValidateHediffPartIntegrity(TickManager __instance)
//		{
//			try
//			{
//				if(__instance.TicksGame % (5 * GenTicks.TickLongInterval) == 0)
//				{
//					IEnumerable<Pawn> allPawns = Find.CurrentMap?.mapPawns?.AllPawns;
//					if(allPawns.EnumerableNullOrEmpty())
//					{
//						return;
//					}
//					//Log.Message("All pawns: " + String.Join(", ", allPawns.Select(p => p.LabelShort)));
//					foreach(Pawn pawn in allPawns)
//					{
//						IEnumerable<Hediff> hediffs = pawn.health?.hediffSet?.hediffs;
//						if (hediffs.EnumerableNullOrEmpty())
//						{
//							continue;
//						}
//						//Log.Message("All hediffs: " + String.Join(", ", hediffs.Select(p => p.def.defName)));
//						foreach (Hediff hediff in hediffs.ToList())
//						{
//							if(hediff.Part == null)
//							{
//								continue;
//							}
//							ValidateHediff(pawn, hediff);
//						}
//					}
//				}
//			}
//			catch (Exception e)
//			{
//				string message = "hediff validation failed \n" + e;
//				Log.ErrorOnce(message, message.GetHashCode());
//			}
//		}
//		public static void ValidateHediff(Pawn pawn, Hediff hediff)
//		{
//			BodyDef body = BodyDefChanger.GetFullStackBody(pawn);
//			//Log.Message("body: " + body?.defName);
//			//Log.Message("current hediff " + hediff.def.defName + " on part: " + hediff.Part.def.defName);
//			//Log.Message("all parts: " + String.Join(", ", body.AllParts.Select(p => p.def.defName)));
//			bool hediffIsOnNonExistantPart = !body.AllParts.Contains(hediff.Part);

//			if (hediffIsOnNonExistantPart)
//			{
//				pawn.health.RemoveHediff(hediff);
//				//Log.Message("Removing incorrect hediff");

//				// band aid solution to be replaced.
//				if (hediff.def.spawnThingOnRemoved != null)
//				{
//					ThingDef thingDefToSpawn = hediff.def.spawnThingOnRemoved;
//					GenSpawn.Spawn(thingDefToSpawn, pawn.Position, pawn.Map);
//				}
//			}
//		}


//		#region attempt 1
//		//public static void ValidateHediff(Pawn pawn, Hediff hediff)
//		//{
//		//	BodyDef body = BodyDefChanger.GetFullStackBody(pawn);
//		//	//Log.Message("body: " + body?.defName);
//		//	//Log.Message("current hediff " + hediff.def.defName + " on part: " + hediff.Part.def.defName);
//		//	//Log.Message("all parts: " + String.Join(", ", body.AllParts.Select(p => p.def.defName)));
//		//	bool hediffIsOnNonExistantPart = !body.AllParts.Contains(hediff.Part);

//		//	if (hediffIsOnNonExistantPart)
//		//	{
//		//		pawn.health.RemoveHediff(hediff);
//		//		RecipeDef recipe;
//		//		string recipeToUse = recipe.addsHediff.defName;
//		//		if ()
//		//		{

//		//		}
//		//		else if (hediff.def.spawnThingOnRemoved != null)
//		//		{
//		//			string thingDefToSpawn = hediff.def.spawnThingOnRemoved.defName;
//		//			//spawn thing def near pawn.
//		//		}
//		//		//Log.Message("Removing incorrect hediff");
//		//	}
//		//}
//		//private static Dictionary<HediffDef, BodyPartDef> _cachedHediffToBodyParts;
//		//private static Dictionary<HediffDef, BodyPartGroupDef> _cachedHediffToBodyGroups;

//		//private static Dictionary<HediffDef, BodyPartDef> CachedHediffToBodyParts
//		//{
//		//	get
//		//	{
//		//		if (_cachedHediffToBodyParts == null)
//		//		{
//		//			CacheHediffRecipes();
//		//		}
//		//		return _cachedHediffToBodyParts;
//		//	}
//		//}
//		//private static Dictionary<HediffDef, BodyPartGroupDef> CachedHediffToBodyGroups
//		//{
//		//	get
//		//	{
//		//		if (_cachedHediffToBodyGroups == null)
//		//		{
//		//			CacheHediffRecipes();
//		//		}
//		//		return _cachedHediffToBodyGroups;
//		//	}
//		//}
//		//private static void CacheHediffRecipes()
//		//{
//		//	foreach (RecipeDef recipe in DefDatabase<RecipeDef>.AllDefsListForReading)
//		//	{
//		//		if (recipe.addsHediff == null)
//		//		{
//		//			continue;
//		//		}
//		//		if (!recipe.appliedOnFixedBodyParts.NullOrEmpty())
//		//		{
//		//			_cachedHediffToBodyParts.Add(recipe.addsHediff,)
//		//		}
//		//	}
//		//}
//		//private static bool TryMoveHediff(Pawn pawn, Hediff hediff)
//		//{

//		//}
//		#endregion
//	}
//}