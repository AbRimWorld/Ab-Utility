﻿using system;
using system.collections.generic;
using system.linq;
using system.text;
using system.threading.tasks;

namespace ab_utility.ability
{
    class nabbersuggestions
    {
        public class mysubclassedability : ability
        {
            enum arrivaleffect
            {
                die,
                fade,
                burst,
                dive
            }

            graphicsource graphicsource;
            graphicdestination graphicdestination;
            traveltype traveltype;
            arrivaleffect arrivaleffect;
            travelfinaleffect travelfinaleffect;

            // i don't care how this method is called or whatever, it's ambiguous to illustrate the difference
            public void arrive()
            {
                switch (arrivaleffect)
                {
                    case arrivaleffect.die:
                        return;
                    case arrivaleffect.burst:
                        // some graphic shit
                        return;
                        ...
        }
            }
        }
        /// /////////////////////////////////////////////////////////////////////
        public class myothersubclassedability : ability
        {
            abilityextension_graphicsource graphicsource;
            abilityextension_graphicdestination graphicdestination;
            abilityextension_traveltype traveltype;
            abilityextension_arrival arrivaleffect;
            abilityextension_travelfinaleffect travelfinaleffect;

            // i don't care how this method is called or whatever, it's ambiguous to illustrate the difference
            public void arrive()
            {
                arrivaleffect.arrive();    // this is the important part, your ability-subclass does not give a shit what actually happens, it's enclosed in the arrivaleffect class, which follows encapsulation and makes you a *good boy*
            }
        }

        // abstract classes
        public abstract class abilityextension_graphicsource
        {
            public abstract intvec3 getposition();
        }

        public abstract class abilityextension_graphicdestination
        {
            public abstract intvec3 getposition();
        }

        public abstract class abilityextension_traveltype
        {
            // this is probably the most difficult one, i'd pre-calculate the entire flight path and save it in a register of "key positions" that you fly between, so all you need to do during animation is get the current target key position and fly to it, until you reach it - and then fly to the next / arrive at destination
            public abstract list<intvec> getkeypositions();
        }

        public abstract class abilityextension_arrival
        {
            public abstract void arrive();
        }

        public abstract class abilityextension_travelfinaleffect
        {
            public abstract void finaleffect();
        }

        // concrete classes
        public class abilityextension_arrivaldie : abilityextension_arrival
        {
            override public void arrive()
            {
                return;
            }
        }

        public class abilityextension_arrivalburst : abilityextension_arrival
        {
            override public void arrive()
            {
                // some graphic shit
                return;
            }
        }
    }
}
