﻿//using RimWorld;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Verse;
//using Verse.AI;

//namespace Ab_Utility
//{
//	public class JobDriver_DrainSoul : JobDriver
//	{
//		TargetIndex drainTargetIndex = TargetIndex.A;
//		Thing DrainTarget => base.job.GetTarget(drainTargetIndex).Thing;
//		const int minGrassCount = 5;
//		public override bool TryMakePreToilReservations(bool errorOnFailed)
//		{
//			if (DrainTarget is Plant)
//			{
//				CompSpawnSubplant comp = DrainTarget.TryGetComp<CompSpawnSubplant>();
//				if (comp == null)
//				{
//					return false;
//				}
//				return base.pawn.Reserve(DrainTarget, base.job);
//			}
//			else if (DrainTarget is Pawn targetPawn)
//			{
//				bool? canDrainPawn = pawn.def.GetModExtension<Extension_SoulDrainer>()?.SoulDrainerFor(pawn)?.CanDrain(pawn, targetPawn);
//				if (canDrainPawn.HasValue && canDrainPawn.Value == false)
//				{
//					return false;
//				}
//				return base.pawn.Reserve(targetPawn, base.job);
//			}
//			return false;
//		}

//		protected override IEnumerable<Toil> MakeNewToils()
//		{
//			yield return Toils_Goto.GotoThing(drainTargetIndex, PathEndMode.Touch);
//			yield return Toils_General.WaitWith(drainTargetIndex, 300, true);
//			Toil toil = new Toil();
//			if (DrainTarget is Plant)
//			{
//				toil.AddFinishAction(DrainAnimaTree);
//			}
//			else if (DrainTarget is Pawn targetPawn)
//			{
//				toil.AddFinishAction(DrainPawn);
//			}
//			else
//			{
//				this.FailOn(() => true);
//			}
//			toil.defaultCompleteMode = ToilCompleteMode.Instant;
//			toil.FailOnDespawnedOrNull(drainTargetIndex);
//			yield return toil;
//		}
//		private void DrainAnimaTree()
//		{
//			CompSpawnSubplant comp = DrainTarget.TryGetComp<CompSpawnSubplant>();
//			int animaGrassCount = comp.SubplantsForReading.Count;
//			if (animaGrassCount > minGrassCount-1)
//			{
//				IEnumerable<Thing> grass = comp.SubplantsForReading.Take(minGrassCount);
//				foreach (Thing thing in grass)
//				{
//					thing.Destroy();
//				}
//				comp.Cleanup();//resync active grass count.
//			}
//			else
//			{
//				IntVec3 postision = DrainTarget.Position; 
//				Map map = DrainTarget.Map;
//				DrainTarget.Destroy();
//				GenSpawn.Spawn(ThingDefOf.BurnedTree, postision, map);

//				base.pawn.needs.mood.thoughts.memories.RemoveMemoriesOfDef(Common.AnimaScream);
//			}
//			base.job.verbToUse.TryStartCastOn(DrainTarget);
//		}
//		private void DrainPawn()
//		{
//			//HealthUtility.AdjustSeverity
//			Pawn targetPawn = DrainTarget as Pawn;
//			HealthUtility.AdjustSeverity(targetPawn, HediffDefOf.LoveEnhancer, 0.3f);
//			HealthUtility.AdjustSeverity(base.pawn, HediffDefOf.Carcinoma, 0.3f);
//		}
//	}
//}
