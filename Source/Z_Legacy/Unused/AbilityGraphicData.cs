﻿using System;
using Verse;
using RimWorld;
using Verse.AI;
using System.Collections.Generic;

namespace Ab_Utility
{
	public class AbilityGraphicData : Ability
	{
		/*
		//[NoTranslate]
		//public string ProjectileTexturePath;//Path to Texture that will be flying
		//enum graphicSource 
		//{ 
		//	caster = 0,
		//	midway = 1,
		//	target = 3,
		//	sky = 4,
		//	nearestMadEdge = 5,
		//	randomMapEdge = 6
		//}
		//enum graphicDestination
		//{
		//	caster = 0,
		//	midway = 1,
		//	target = 3,
		//	sky = 4,
		//	nearestMadEdge = 5,
		//	randomMapEdge = 6
		//}
		//enum travelEffect
		//{
		//	stright = 0,
		//	highArch = 1,//zenith is the projectile flight %
		//	lowarch = 2,
		//	zigzag = 3,
		//	wildArch = 4,//arch direction is random.
		//	wildStraight = 5,//random direction // to be used with die, fade, or burst.
		//	chaoticWild = 6//random direction, random zigzag// to be used with die, fade, or burst.
		//}
		//float projectileFlight;// % of distance from source to destination
		//enum arrivalEffect//when it reaches projectileFlight 
		//{
		//	die = 0,
		//	fade = 1,
		//	burst = 2,
		//	dive = 3,
		//	hover = 4,
		//	circleAround = 5//circles around cell or pawn that is the target.
		//}
		//int holdEffectTicks = 0;//a pause holding the ability effect in place before moving to the next step. to be used with hover.
		//int MaintainEffectTIcks = 0;//keep rendering effect while proceeding.
		//[NoTranslate]
		//public string ProjectileMaintainTexturePath;//to be used with hover
		//[NoTranslate]
		//public string ProjectileShiftTexturePath;//change texture of projectile. if empty keep original
		//enum travelFinalEffect
		//{
		//	stright = 0,
		//	zigzag = 1,
		//	wildArch = 2,
		//	wildStraight = 3,//random direction // to be used with die, fade, or burst.
		//	chaoticWild = 4//random direction, random zigzag// to be used with die, fade, or burst.
		//}
		*/
	/*
	//	AbilityExtension_GraphicSource graphicSource;
	//	AbilityExtension_GraphicDestination graphicDestination;
	//	AbilityExtension_TravelType travelType;
	//	AbilityExtension_Arrival arrivalEffect;
	//	AbilityExtension_TravelFinalEffect travelFinalEffect;

	//	// I don't care how this method is called or whatever, it's ambiguous to illustrate the difference
	//	public void Arrive()
	//	{
	//		arrivalEffect.Arrive();// this is the important part, your Ability-subclass does not give a shit what actually happens, it's enclosed in the arrivalEffect class, which follows encapsulation and makes you a *good boy*
	//	}
	//}

	//// abstract classes
	//public abstract class AbilityExtension_GraphicSource
	//{
	//	public abstract IntVec3 GetPosition();
	//}

	//public abstract class AbilityExtension_GraphicDestination
	//{
	//	public abstract IntVec3 GetPosition();
	//}

	//public abstract class AbilityExtension_TravelType
	//{
	//	// this is probably the most difficult one, I'd pre-calculate the entire flight path and save it in a register of "key positions" that you fly between, so all you need to do during animation is get the current target key position and fly to it, until you reach it - and then fly to the next / arrive at destination
	//	public abstract List<IntVec3> GetKeyPositions();
	//}

	//public abstract class AbilityExtension_Arrival
	//{
	//	public abstract void Arrive();
	//}

	//public abstract class AbilityExtension_TravelFinalEffect
	//{
	//	public abstract void FinalEffect();
	//}

	//// concrete classes
	//public class AbilityExtension_ArrivalDie : AbilityExtension_Arrival
	//{
	//	override public void Arrive()
	//	{
	//		return;
	//	}
	//}

	//public class AbilityExtension_ArrivalBurst : AbilityExtension_Arrival
	//{
	//	override public void Arrive()
	//	{
	//		// some graphic shit
	//		return;
	//	}
	*/
	
	}
}