﻿//using HarmonyLib;
//using RimWorld;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using UnityEngine;
//using Verse;
//using Verse.AI;

//namespace Ab_Utility
//{
//	[HarmonyPatch(typeof(Plant), "GetGizmos")]
//	public class AnimaTreeGizmo_Patch
//	{
//		[HarmonyPostfix]
//		public static IEnumerable<Gizmo> AddDrainAnimaTreeGizmo(IEnumerable<Gizmo> __result, Plant __instance)
//		{
//			foreach (Gizmo gizmo in __result)
//			{
//				yield return gizmo;
//			}
//			if (__instance.def != ThingDefOf.Plant_TreeAnima)
//			{
//				yield break;
//			}

//			yield return GetGizmoFor(__instance);
//		}
//		private static Gizmo cachedGizmo;
//		private static Plant cachedPlant;
//		private static Gizmo GetGizmoFor(Plant plant)
//		{
//			if (cachedPlant == null || cachedPlant != plant)
//			{
//				RecacheGizmoFor(plant);
//			}
//			return cachedGizmo;
//		}
//		private static void RecacheGizmoFor(Plant plant)
//		{
//			//Log.Message(plant.ToString());
//			cachedPlant = plant;
//			cachedGizmo = new Command_Action
//			{
//				defaultLabel = "Ab_Drain_AnimaTree".Translate(),
//				defaultDesc = "Ab_Drain_AnimaTree".Translate(),
//				icon = ContentFinder<Texture2D>.Get(("UI/Icons/Rituals/AnimaTreeLinking"), true),
//				action = delegate
//				{
//					new PawnSelector_Window(plant.Map.mapPawns.FreeColonists, OrderDrain);
//				}
//			};
//		}
//		private static void OrderDrain(Pawn pawn)
//		{
//			Job job = JobMaker.MakeJob(Ab_JobDefOf.Ab_DrainSoul, cachedPlant);
//			job.count = 1;
//			pawn.jobs.TryTakeOrderedJob(job);
//		}
//	}
//}