﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Ab_Utility
{
	public class AbilityInteractionSelector_Window : Window
	{
		private Vector2 scrollPos;
		private float scrollHeight = float.MaxValue;

		Ability ability;
		Pawn pawn;
		Action extraSelectionAction;
		private List<AbilityInteractionEntry> entries = new List<AbilityInteractionEntry> ();

		public AbilityInteractionSelector_Window(Ability ability, Pawn pawn, Action extraSelectionAction = null)
		{
			this.ability = ability;
			this.pawn = pawn;
			this.extraSelectionAction = extraSelectionAction;

			forcePause = true;
			draggable = true;
			doCloseX = true;
			preventCameraMotion = false;

			Find.WindowStack.Add(this);

			InitEntries();
		}

		private void InitEntries()
		{
			// Gain Psylink || Upgrade psylink level
			string gainPsylinkLabel = "Ab_gainPsylink".Translate();
			Action<Pawn> gainPsylinkAction = (Pawn pawn) =>
			{
				//Log.Message(pawn.LabelShort);
				Ab_AbilityUtility.GainPsylink(pawn);
			};
			Func<Pawn, string> gainPsylinkCheck = (Pawn pawn) =>
			{
				if (pawn.health?.hediffSet == null)
				{
					return "Ab_noHealth".Translate();
				}
				Hediff psylinkHediff = pawn.health.hediffSet.GetFirstHediffOfDef(HediffDefOf.PsychicAmplifier);
				if (psylinkHediff != null)
				{
					if (psylinkHediff.Severity >= HediffDefOf.PsychicAmplifier.maxSeverity)
					{
						return "Ab_alreadyMax".Translate();
					}
				}
				return null;
			};
			
			AbilityInteractionEntry gainPsylinkEntry = new AbilityInteractionEntry(this, Textures.TreeToPsylink, gainPsylinkLabel, gainPsylinkAction, extraSelectionAction, gainPsylinkCheck);
			entries.Add(gainPsylinkEntry);

			//Gain Ability
			string gainPsycastAbilityLabel = "Ab_gainAbility".Translate();
			Action<Pawn> gainPsycastAbilityAction = (Pawn pawn) =>
			{
				//Log.Message(pawn.LabelShort);
				Hediff_Psylink psylinkHediff = pawn.health.hediffSet.GetFirstHediff<Hediff_Psylink>();
				if (psylinkHediff == null)
				{
					Log.Error("no psylinkHediff");
					return;
				}
				Ab_AbilityUtility.GainRandomPsylinkAbility(pawn, psylinkHediff.level);
			};
			Func<Pawn, string> gainPsycastAbilityRequirement = (Pawn pawn) =>
			{
				if (pawn.health?.hediffSet == null)
				{
					return "Ab_noHealth".Translate();
				}
				Hediff_Psylink psylinkHediff = pawn.health.hediffSet.GetFirstHediff<Hediff_Psylink>();
				if (psylinkHediff == null)
				{
					return "Ab_noPsylinkHediff".Translate();
				}
				if (Ab_AbilityUtility.PsylinkAbilityFor(pawn, psylinkHediff.level) == null)
				{
					return "Ab_alreadyHasAllAbilities".Translate();
				}
				return null;
			};
			AbilityInteractionEntry gainPsycastAbilityDefEntry = new AbilityInteractionEntry(this, Textures.TreeToAbility, gainPsycastAbilityLabel, gainPsycastAbilityAction, extraSelectionAction, gainPsycastAbilityRequirement);
			entries.Add(gainPsycastAbilityDefEntry);

			//Gain Buff
			string gainBuffLabel = "Ab_gainBuff".Translate();
			Action<Pawn> gainBuffAction = (Pawn pawn) =>
			{
				HealthUtility.AdjustSeverity(pawn, Common.Ab_DrainBuff, 0.5f);
				if (pawn.HasPsylink && pawn.psychicEntropy.NeedsPsyfocus) // should this be only for the buff or also for the other options ?
				{
					pawn.psychicEntropy.OffsetPsyfocusDirectly(0.2f); // use a flat value or a varible ?: change to variable if to be used in other options.
				}
			};
			Func<Pawn, string> gainBuffRequirement = (Pawn pawn) =>
			{
				if (pawn.health?.hediffSet == null)
				{
					return "no health".Translate();
				}
				return null;
			};
			AbilityInteractionEntry gainPsycastBuffEntry = new AbilityInteractionEntry(this, Textures.AnimaBuff, gainBuffLabel, gainBuffAction, extraSelectionAction, gainBuffRequirement);
			entries.Add(gainPsycastBuffEntry);

			//Drain Anima Soul (Later)

			//Deposit Anima Soul (Later)
		}

		public override void DoWindowContents(Rect inRect)
		{
			UI_Utility.MakeAndBeginScrollView(inRect, scrollHeight, ref scrollPos, out Listing_Standard list);
			list.HeaderLabel("Ab_Select_Action".Translate());
			foreach(AbilityInteractionEntry entry in entries)
			{
				entry.Draw(pawn, list);
			}

			UI_Utility.EndScrollView(list, ref scrollHeight);
		}
		public override Vector2 InitialSize => new Vector2(650, 320); // y = 600 is good for 5 options ... First X value = 800 a bit wide
	}

	public class AbilityInteractionEntry
	{
		Window window;
		Texture2D icon;
		string label;
		Action<Pawn> selectionAction;
		Action extraSelectionAction;
		/// <summary>
		/// If this evaluates to null, the entry is valid.
		/// </summary>
		Func<Pawn, string> requirementFunc;
		
		const float selectionEntryHeight = 80;
		const float labelWidthRation = 0.7f;

		/// <summary>
		/// If <paramref name="requirementFunc"/> evaluates to null, the entry is valid.
		/// </summary>
		public AbilityInteractionEntry(Window window, Texture2D icon, string label, Action<Pawn> selectionAction, Action extraSelectionAction = null, Func<Pawn, string> requirementFunc = null)
		{
			this.window = window;
			this.icon = icon;
			this.label = label;
			this.selectionAction = selectionAction;
			this.extraSelectionAction = extraSelectionAction;
			this.requirementFunc = requirementFunc;
		}
		public void Draw(Pawn pawn, Listing_Standard list)
		{
			Rect rowRect = list.GetRect(selectionEntryHeight);
			Widgets.DrawHighlightIfMouseover(rowRect);
			Color previousColor = GUI.color;
			string requirementInvalidReason = requirementFunc(pawn);
			bool isRequirementValid = requirementInvalidReason == null;
			if (!isRequirementValid)
			{
				GUI.color = Color.gray;
			}
			else 
			{
				if (Widgets.ButtonInvisible(rowRect))
				{
					selectionAction(pawn);
					extraSelectionAction();
					window.Close();
				}				
			}
			
			Rect iconRect = new Rect(rowRect.xMin, rowRect.yMin, rowRect.height, rowRect.height);
			Widgets.DrawTextureFitted(iconRect, icon, 1);

			float totalLabelWidth = rowRect.width - selectionEntryHeight;
			Rect labelRect = new Rect(iconRect.xMax, rowRect.yMin, totalLabelWidth * labelWidthRation, rowRect.height);
			UI_Utility.LabelInCenter(labelRect, label, out _);
			
			Rect requirementRect = new Rect(labelRect.xMax, rowRect.yMin, totalLabelWidth * (1 - labelWidthRation), rowRect.height);
			string requirementLabel = requirementFunc == null ? "" : requirementFunc(pawn);
			UI_Utility.LabelInCenter(requirementRect, requirementLabel, out _, 0, GameFont.Tiny);
			
			GUI.color = previousColor;
		}

	}
}