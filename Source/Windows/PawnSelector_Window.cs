﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Ab_Utility
{
	public class PawnSelector_Window : Window
	{
		private Vector2 scrollPos;
		private float scrollHeight = float.MaxValue;
		List<Pawn> pawns = new List<Pawn>();
		const float pawnEntryHeight = 80;
		Action<Pawn> selectionAction;
		public PawnSelector_Window(List<Pawn> pawns, Action<Pawn> selectionAction)
		{
			this.pawns = pawns;
			draggable = true;
			doCloseX = true;
			preventCameraMotion = false;
			Find.WindowStack.Add(this);
			this.selectionAction = selectionAction;
		}
		public override void DoWindowContents(Rect inRect)
		{
			UI_Utility.MakeAndBeginScrollView(inRect, scrollHeight, ref scrollPos, out Listing_Standard list);

			list.HeaderLabel("Ab_Select_Pawn".Translate());
			foreach(Pawn pawn in pawns)
			{
				DrawPawn(pawn, list);
			}

			UI_Utility.EndScrollView(list, ref scrollHeight);
		}
		public override Vector2 InitialSize => new Vector2(220, 600);

		private void DrawPawn(Pawn pawn, Listing_Standard list)
		{
			Rect rowRect = list.GetRect(pawnEntryHeight);
			Widgets.DrawHighlightIfMouseover(rowRect);
			if (Widgets.ButtonInvisible(rowRect))
			{
				selectionAction.Invoke(pawn);
				Close();
			}

			Rect portraitRect = new Rect(rowRect.xMin, rowRect.yMin, pawnEntryHeight, pawnEntryHeight);//square
			RenderTexture portraitTexture = PortraitsCache.Get(pawn, portraitRect.size, Rot4.South);
			Widgets.DrawTextureFitted(portraitRect, portraitTexture, 1);

			Rect labelRect = new Rect(portraitRect.xMax, portraitRect.yMin, rowRect.width - portraitRect.width, rowRect.height);
			//Widgets.Label(labelRect, pawn.LabelShort);
			UI_Utility.LabelInCenter(labelRect, pawn.LabelShort, out _);

		}
	}
}